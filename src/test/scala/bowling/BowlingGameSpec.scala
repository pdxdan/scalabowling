package bowling

import org.scalatest.{Matchers, FunSpec}

/**
 * Created by Dan Anolik on 10/23/15.
 * Tests for the BowlingGame score calculator
 */

class BowlingGameSpec extends FunSpec with Matchers {

  val bgame = new BowlingGame

  describe("A bowling score tracker") {
    it("should identify a strike if the first roll in a frame was 10") {
      bgame.isStrike(List(10,0,5,6)) should be (true)
      bgame.isStrike(List(0,10,5,6)) should be (false)
    }

    it("should identify a spare from two rolls adding up to 10, if the first roll was not a 10") {
      bgame.isSpare(List(10  )) should be (false)
      bgame.isSpare(List(10,0)) should be (false)
      bgame.isSpare(List(1,9 )) should be (true)
      bgame.isSpare(List(0,10)) should be (true)
    }

    it("should only score two rolls if a frame is less than ten") {
      bgame.rollsForFrame(List(4,5,6,7)) should be (List(4,5))
    }

    it("should add the next roll to the score after a spare"){
      bgame.rollsForFrame(List(1,9,6,7)) should be (List(1,9,6))
    }

    it("should add the next two rolls to the score after a strike"){
      bgame.rollsForFrame(List(10,9,6,7)) should be (List(10,9,6))
    }

    it("should add and extra roll if the tenth frame was a spare") {
      bgame.score(List.fill(18)(1) ++ List(4,6)) should be(28)
      bgame.score(List.fill(18)(1) ++ List(4,6,1)) should be(29)
    }

    it("should add two extra rolls if the tenth frame was a strike") {
      bgame.score(List.fill(18)(1) ++ List(10)) should be(28)
      bgame.score(List.fill(18)(1) ++ List(10,1)) should be(29)
      bgame.score(List.fill(18)(1) ++ List(10,1,1)) should be(30)
    }

    it("should end the scoring after 10 frames") {
      bgame.score(List.fill(24)(1)) should be(20)                   // should only count the first 20 balls
      bgame.score(List.fill(18)(1) ++ List(4,6,1,1)) should be(29)  // should only count the first 21 balls
      bgame.score(List.fill(18)(1) ++ List(10,1,1,1)) should be(30) // should only count the first 22 balls
    }

  }
  describe("A final bowling game score") {
    it("should be zero if no pins are knocked down") {
      bgame.score(List(0,0,0,0)) should equal(0)
    }

    it("should be 20 if all rolls knocked down a single pin") {
      bgame.score(List.fill(20)(1)) should be(20)
    }

    it("should be 150 if all rolls knocked down 5 pins") {
      bgame.score(List.fill(21)(5)) should be(150)
      bgame.score(List.fill(22)(5)) should be(150)
    }

    it("should be 300 when bowling all strikes") {
      bgame.score(List.fill(12)(10)) should be(300)
    }
  }
}
