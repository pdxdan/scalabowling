package bowling

/**
 * Created by Dan Anolik on 10/23/15 for a functional programming exercise
 * A functional programming approach to the classic bowling game score calculator
 */

class BowlingGame {

  type IntList = List[Int]

  def isStrike(rolls: IntList): Boolean = {
    rolls.head == 10
  }

  def isSpare(rolls: IntList): Boolean = {
    rolls.length > 1 &&                 //ensure that I'm adding up at least two rolls
    rolls.head != 10 &&                 //exclude strikes
    rolls.head + rolls.tail.head == 10  //add the top two rolls of the list together
  }

  def rollsForFrame(rolls: IntList): IntList = {
    if (isStrike(rolls) || isSpare(rolls)) rolls.take(3)
    else rolls.take(2)
  }

  def remainingRolls(rolls: IntList): IntList = {
    if (isStrike(rolls)) rolls.drop(1)
    else rolls.drop(2)
  }

  def toFrames(rolls: IntList, frames: IntList): (IntList,IntList) = {
    val f = frames ++ List(rollsForFrame(rolls).sum)
    if (remainingRolls(rolls).isEmpty) (null, f)
    else toFrames(remainingRolls(rolls), f)
  }

  def score(rolls: IntList): Int = {
    toFrames(rolls, List())._2.take(10).sum
  }

}
