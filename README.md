# ScalaBowling README #

ScalaBowling is a functional programming approach to the classic bowling game score calculator, and a good excuse to practice my Scala. 

The approach was inspired by Jeremy Neander's Bowling Game Kata in Clojure:
https://vimeo.com/50459431


The project uses SBT for build and test. The test output looks like this (on my machine):

```
#!testoutput

[info] BowlingGameSpec:
[info] A bowling score tracker
[info] - should identify a strike if the first roll in a frame was 10
[info] - should identify a spare from two rolls adding up to 10, if the first roll was not a 10
[info] - should only score two rolls if a frame is less than ten
[info] - should add the next roll to the score after a spare
[info] - should add the next two rolls to the score after a strike
[info] - should add and extra roll if the tenth frame was a spare
[info] - should add two extra rolls if the tenth frame was a strike
[info] - should end the scoring after 10 frames
[info] A final bowling game score
[info] - should be zero if no pins are knocked down
[info] - should be 20 if all rolls knocked down a single pin
[info] - should be 150 if all rolls knocked down 5 pins
[info] - should be 300 when bowling all strikes
[info] Run completed in 442 milliseconds.
[info] Total number of tests run: 12
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 12, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
```